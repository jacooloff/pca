#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import numpy as np

#Чтение данных

my_data = np.genfromtxt('creditcard.csv', delimiter=',', skip_header=1 )

#Отделение классов
my_data = np.delete(my_data, 30, axis=1)

#Ковариационная матрица
cov_mat = np.cov(my_data.T)
print('Ковариационная матрица{}:\n'.format(cov_mat.shape), cov_mat)

#Собственное значение и вектор из ковариационной матрицы
eig_val_cov, eig_vec_cov = np.linalg.eig(cov_mat)

for i in range(len(eig_val_cov)):

    eigvec_cov = eig_vec_cov[:,i].reshape(1,30).T


    print('Собственный вектор {}: \n{}'.format(i+1, eigvec_cov))

    print('Собственное значение {} из ковариационной матрицы: {}'.format(i+1, eig_val_cov[i]))

    print(40 * '-')
